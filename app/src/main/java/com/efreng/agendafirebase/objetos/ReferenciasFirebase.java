package com.efreng.agendafirebase.objetos;

public class ReferenciasFirebase {
    final static public String URL_DATABASE = "https://agendafirebase-cf35e.firebaseio.com/";
    final static public String DATABASE_NAME = "agenda";
    final static public String TABLE_NAME = "contactos";
}
